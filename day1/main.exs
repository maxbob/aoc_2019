defmodule Max do
  def compute_fuel(dist) do
    Kernel.trunc(dist / 3 - 2)
  end

  def map_cf(inputStr) do
    sums = Enum.map(
      inputStr,
      fn numS ->
        {num, ""} = Integer.parse(numS)
        compute_fuel(num)
      end)
    Enum.sum(sums)
  end

  def fuel_module(fuel) do
    cf = compute_fuel(fuel)
    if cf > 0 do
      cf + fuel_module(cf)
    else
      0
    end
end
  
  def map_fm(inputStr) do
    sums = Enum.map(
      inputStr,
      fn numS ->
        {num, ""} = Integer.parse(numS)
        fuel_module(num)
      end)
    Enum.sum(sums)
  end
    

  def day1() do
    input = String.split(File.read!("input"))
    day1a = map_cf(input)
    IO.inspect(day1a)
    day1b = map_fm(input)
    IO.inspect(day1b)
  end
end

Max.day1()
