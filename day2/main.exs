defmodule Max do

  def state_to_arr(state) do
    arr = String.split(state, ",")
    for s <- arr do
      {i, ""} = Integer.parse(s)
      i
    end
  end

  def arr_to_state(arr) do
    Enum.join(arr, ",")
  end

  def proc(loc, state_arr) do
    op = Enum.slice(state_arr, loc, 4)
    op_num = Enum.at(op, 0)
    case op_num do
      1 ->
        add_op(op, state_arr)
      2 ->
        mult_op(op, state_arr)
      99 ->
        state_arr
      _ ->
        state_arr
    end
  end

  def add_op(op, state_arr) do
    a = Enum.at(state_arr, Enum.at(op, 1))
    b = Enum.at(state_arr, Enum.at(op, 2))
    List.replace_at(state_arr, Enum.at(op, 3), a + b)
  end

  def mult_op(op, state_arr) do 
    a = Enum.at(state_arr, Enum.at(op, 1))
    b = Enum.at(state_arr, Enum.at(op, 2))
    List.replace_at(state_arr, Enum.at(op, 3), a * b)
  end

  def step(loc, state) do
    state_arr = state_to_arr(state)
    if loc > Kernel.length(state_arr) do
      state
    else
      iter_state = proc(loc, state_arr)
      step(loc+4, arr_to_state(iter_state))
    end
  end

  def part1() do
    #test1 = "1,9,10,3,2,3,11,0,99,30,40,50"
    #Max.step(0, test1)
    #test2 = "1,1,1,4,99,5,6,0,99"
    #Max.step(0, test2)
    test3 = "1,12,2,3,1,1,2,3,1,3,4,3,1,5,0,3,2,1,10,19,1,19,5,23,2,23,9,27,1,5,27,31,1,9,31,35,1,35,10,39,2,13,39,43,1,43,9,47,1,47,9,51,1,6,51,55,1,13,55,59,1,59,13,63,1,13,63,67,1,6,67,71,1,71,13,75,2,10,75,79,1,13,79,83,1,83,10,87,2,9,87,91,1,6,91,95,1,9,95,99,2,99,10,103,1,103,5,107,2,6,107,111,1,111,6,115,1,9,115,119,1,9,119,123,2,10,123,127,1,127,5,131,2,6,131,135,1,135,5,139,1,9,139,143,2,143,13,147,1,9,147,151,1,151,2,155,1,9,155,0,99,2,0,14,0"
    ans = Max.step(0, test3)
    IO.inspect(Enum.at(state_to_arr(ans), 0))
  end

  def solve_it(in1, in2) do
    orig = "1,12,2,3,1,1,2,3,1,3,4,3,1,5,0,3,2,1,10,19,1,19,5,23,2,23,9,27,1,5,27,31,1,9,31,35,1,35,10,39,2,13,39,43,1,43,9,47,1,47,9,51,1,6,51,55,1,13,55,59,1,59,13,63,1,13,63,67,1,6,67,71,1,71,13,75,2,10,75,79,1,13,79,83,1,83,10,87,2,9,87,91,1,6,91,95,1,9,95,99,2,99,10,103,1,103,5,107,2,6,107,111,1,111,6,115,1,9,115,119,1,9,119,123,2,10,123,127,1,127,5,131,2,6,131,135,1,135,5,139,1,9,139,143,2,143,13,147,1,9,147,151,1,151,2,155,1,9,155,0,99,2,0,14,0"
    arr = state_to_arr(orig)
    arr = List.replace_at(arr, 1, in1)
    arr = List.replace_at(arr, 2, in2)
    result = step(0, arr_to_state(arr))
    result_arr = state_to_arr(result)
    Enum.at(result_arr, 0)
  end

  def part2() do
    for i <- 0..100,
        j <- 0..100 do
          if solve_it(i, j) == 19690720 do
            IO.inspect(i)
            IO.inspect(j)
            IO.inspect(solve_it(i, j))
          else
            nil
          end
        end
  end

end

Max.part1()
Max.part2()
