### Advent of Code 2019 in Elixir

This is my first time using a functional language for something. It's guaranteed to be a disaster
but I'm hoping to have added a language to my arsenal by the end of the month. 

Developing using WSL. So I just followed [these instructions for Ubuntu](https://elixir-lang.org/install.html#unix-and-unix-like), and had elixir up and running quickly.

Should just have to run:
```bash
elixir main.exs
```
to see the answers. Ideally I'll have a `test.exs` file which will include the various sample inputs they give
for validation.
